/* Moscow Institute of Electronics and Mathematics
   The Faculty of Applied Mathematics

   Assignment #9
   Language: C99
   Compiler: gcc

   Student: Pavel Borisov
   Group: Applied Informatics 11
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#define INTBUFSIZE 10
/** writeInt
 *
 * writes a number of binary int objects into a file
 * returns a number of written objects
 */
size_t  writeInt (size_t n, int* data, FILE *file);

/** readInt
 *
 * reads all the binary int objects into data array from a file,
 * storing the number of objects in size (not null)
 *
 * returns array of objects
 */
int* readInt(size_t *size,  FILE *file);

/** printBinaryData
 *
 *  print binary data from fd to stdin
 */

void printBinaryData (FILE* fd);
