/* Moscow Institute of Electronics and Mathematics
   The Faculty of Applied Mathematics

   Assignment #9
   Language: C99
   Compiler: gcc

   Student: Pavel Borisov
   Group: Applied Informatics 11
*/

#include "binint.h"
size_t  writeInt (size_t n, int* data, FILE* file)
{
  size_t written = fwrite(data, sizeof(int), n, file);
  if (written != n)
    //call to strerror to determine the error
    fprintf(stderr, "writeInt: I/O Error: %s\n", strerror(errno));

  clearerr(file);
  return written;
}

int* readInt (size_t *size, FILE *file)
{
  *size = INTBUFSIZE;
  int * data = calloc(INTBUFSIZE, sizeof(int));

  for (size_t pos = 0;;)
    {
      pos = fread(data, sizeof(int), INTBUFSIZE, file);
      if (feof(file))
        {
          if (pos < *size)
            {
              data = realloc(data, pos*sizeof(int));
              *size = pos;
            }
          clearerr(file);
          break;
        }
      else if (ferror(file))
        {
          fprintf(stderr, "readInt: I/O Error: %s\n", strerror(errno));
          *size = 0;
          clearerr(file);
          return NULL;
        }
      else //there's more
        {
          *size += INTBUFSIZE;
          data = realloc(data, (*size)*sizeof(int));
        }
    }
  return data;
}

void printBinaryData (FILE* fd)
{
  size_t size;
  int *data = readInt(&size, fd);
  for (size_t i = 0; i < size; ++i)
    {
      printf("%d ", data[i]);
    }
  free(data);
  printf("\n");
}
