/* Moscow Institute of Electronics and Mathematics
   The Faculty of Applied Mathematics

   Assignment #10
   Language: C99
   Compiler: gcc

   Student: Pavel Borisov
   Group: Applied Informatics 11
*/


#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define BUFFERSIZE 100
#define LINES 10

/** getSubstring
 *
 * get substring to filter from stdin
 */


char * getSubstring();

/** getLinesFromFile
 *
 * get char** array of all the strings (size would be written in size variable
 */
char ** getLinesFromFile(size_t *size, const char *fileName, const char *substring);

/** getLine
 *
 * get a single line from file fd (putting size in respective variable)
 * On EOF return NULL
 */

char* getLine(int *size, FILE *fd);

int filter(const char *source, const char *dest, const char *substr);

void printUsage(const char* progname);
