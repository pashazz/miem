/* Moscow Institute of Electronics and Mathematics
   The Faculty of Applied Mathematics

   Assignment #10
   Language: C99
   Compiler: gcc

   Student: Pavel Borisov
   Group: Applied Informatics 11
*/

#include "lab10.h"
#include <errno.h>

int main(int argc, char* argv[])
{
  if ((argc == 2) || (argc == 3))
    {
      size_t size;
      char *substr = getSubstring();
      if (substr)
        return filter(argv[1], argc == 3 ? argv[2] : "stdout", substr);
    }
  else
    {
      printUsage(argv[0]);
      return -1;
    }
  return 0;
}

int filter(const char *source, const char *dest, const char *substr)
{
  FILE *fd_src = fopen(source, "r");
  if (!fd_src)
    {
      fprintf(stderr, "filter: I/O error: can't open source file: %s\n", strerror(errno));
      return errno;
    }

  FILE *fd_dest;
  if (strcmp(dest, "stdout") == 0)
    fd_dest = stdout;
  else
    fd_dest = fopen(dest, "w");
  if (!fd_dest)
    {
      fprintf(stderr, "filter: I/O error: can't open destination file: %s\n", strerror(errno));
      return errno;
    }

  int size;
  char *line;
  while(line = getLine(&size, fd_src))
    {
      if (strstr(line, substr))
        fputs(line, fd_dest);
      free(line);
    }
  return 0;
}

char * getSubstring()
{
  printf("Enter substring to search (up to %d symbols):\n", BUFFERSIZE-1);
  char *line = calloc(BUFFERSIZE, sizeof(char));
  line = fgets(line, BUFFERSIZE, stdin);
  //remove \n if present
  int i = strlen(line) - 1;
  if (line[i] == '\n')
    line[i] = '\0';
  return line;
}


char* getLine(int *size, FILE *fd)
{
  int len = 0;
  char *line = calloc(BUFFERSIZE, sizeof(char));
  len = BUFFERSIZE;
  if (fgets(line, len, fd) == NULL) //EOF
    {
      free(line);
      return NULL;
    }
  else
    {
      int n = strlen(line);
      if (line[n-1] == '\n')
        {
          len = n+1;
          if (len != BUFFERSIZE)
            line = realloc(line, len*sizeof(char));
        }
      else //использую конкатенацию
        {
          for (;;)
            {

              char* concatline = calloc(BUFFERSIZE, sizeof(char));
              if (fgets(concatline, BUFFERSIZE, fd) == NULL) //EOF
                {
                  free(concatline);
                  break;
                }
              int concatsize = strlen(concatline); //we've already got \0 into account
              len += concatsize;
              line = realloc(line, len*sizeof(char));
              strcat(line, concatline);
              if (concatline[concatsize - 1] == '\n')
                {
                  free(concatline);
                  break;
                }
              else
                free(concatline);
            }
        }
      *size = len;
      return line;
    }
}

void printUsage(const char *progname)
{
  printf("Assignment 10 - grep-like utility\n");
  printf("\tUSAGE:\n");
  printf("\t%s <input file> <output file or stdout>\n", progname);
}
