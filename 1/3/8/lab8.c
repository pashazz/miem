/* Moscow Institute of Electronics and Mathematics
   The Faculty of Applied Mathematics

   Assignment 8
   Language: C99
   Compiler: gcc

   Student: Pavel Borisov
   Group: Applied Informatics 11
*/
#include "lab8.h"

int main()
{
  char *buf = calloc(11, sizeof(char));
  for (;;)
    {
      scanf("%s", buf);
      if (buf[strlen(buf) - 1] == '.')
          break;
      else
        {
          removeFirstLetter(buf);
          printf("%s ", buf);
        }
    }
  free(buf);
  printf("\n");
  return 0;
}

void removeFirstLetter(char* buf)
{
  size_t len = strlen(buf);
  for (size_t i = 1; i <= len; ++i) // \0 should be moved too
      buf[i-1] =  buf[i];
}
