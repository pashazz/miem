/* Moscow Institute of Electronics and Mathematics
   The Faculty of Applied Mathematics

   Assignment #11
   Language: C99
   Compiler: gcc

   Student: Pavel Borisov
   Group: Applied Informatics 11
*/
#include "menu.h"
#include "common.h"
#include <limits.h>

#define PRINTSPEC "|%-4d|%-10s|%-20s|%-20s|%-20s|%-10d|%-10d|%-10d|\n"
#define PRINTSPECLEN 112

void shell(FILE* fd, const char *fileName)
{
  while(1)
    {
        printf(">");
        size_t size;
        char **cmd = command(&size);
        if(!cmd)
          {
            printf("\n");
            continue;
          }
        else if (!strlen(cmd[0]))
          continue;

        if ((!strcmp(cmd[0], "quit")) && size == 1)
          { //quit
            printf("Bye.\n");
            return;
          }
        else if (!strcmp(cmd[0], "help"))
          {//help
            if (size == 1)
              printHelp();
            else if (size == 2)
              printHelpOnModule(cmd[1]);
            else
              printf("Too many arguments\n");
          }
        else if (!strcmp(cmd[0], "add") && size == 1)
          {//add new record
            DatabaseRecord *rec = populateRecord(NULL);
            if (rec)
              addRecord(fd, rec);
            freeRecord(rec);
          }
        else if(!strcmp(cmd[0], "remove"))
          {//delete exactly one record
            if (size == 2)
              {
                char *endptr;
                int pos = (int)strtol(cmd[1], &endptr, 10);
                if (endptr == cmd[1]) //no results, incorrect input
                  {
                    fprintf(stderr, "remove: incorrect argument\n");
                  }
                else
                  removeRecord(fd, pos);
              }
            else
              printf("remove: expected one argument\n");
          }
        else if(!strcmp(cmd[0], "print") && size == 1)
          {//print all records
            printAllRecords(fd);
          }
        else if (!strcmp(cmd[0], "edit"))
          {
            if (size == 2)
              {
                char *endptr;
                int pos = (int)strtol(cmd[1], &endptr, 10);
                if (endptr == cmd[1]) //no results, incorrect input
                  {
                    fprintf(stderr, "edit: incorrect argument\n");
                  }
                else
                  {
                    DatabaseRecord *rec = record(fd, pos);
                    if (rec)
                      {
                        rec = populateRecord(rec);
                        rewriteRecord(fd, pos, rec);
                      }
                  }
              }
            else
              printf("edit: expected one argument\n");
          }
        else if (!strcmp(cmd[0], "find"))
          search(cmd, size, fd);

        else if (!strcmp(cmd[0], "sort"))
          sort(cmd, size, fd);
        else
          printf("Unknown command\n");
        free(cmd);
    }


}

DatabaseRecord* populateRecord(DatabaseRecord *existing)
{
  if (existing)
    {
      printf("Name [%s]:", existing->name);
      char *input = calloc(12, sizeof(char));
      if (strcmp(fgets(input, 12, stdin), "\n"))
        {
          trim(input);
          strncpy(existing->name, input, 11);
        }

      free(input);
      input = calloc(22, sizeof(char));
      printf("Middlename [%s]:", existing->middlename);
      if (strcmp(fgets(input, 22, stdin), "\n"))
        {
          trim(input);
          strncpy(existing->middlename, input, 21);
        }

      free(input);
      input = calloc(22, sizeof(char));
      printf("Surname [%s]:", existing->surname);
      if (strcmp(fgets(input, 22, stdin), "\n"))
        {
          trim(input);
          strncpy(existing->surname, input, 21);
        }

      free(input);
      input = calloc(22, sizeof(char));
      printf("Subdivision [%s]:", existing->subdivision);
      if (strcmp(fgets(input, 22, stdin), "\n"))
        {
          trim(input);
          strncpy(existing->subdivision, input, 21);
        }

      free(input);

      unsigned int numinput;
      int code = 0;
      while (!code)
        {
          printf("Salary [%u]:", existing->salary);
          numinput = getInt(&code);
        }

      if (code == 1)
        existing->salary = numinput;

      code = 0;
      while (!code)
        {
          printf("Premium [%u]:", existing->premium);
          numinput = getInt(&code);
        }

      if (code == 1)
        existing->premium = numinput;

      code = 0;
      while(!code)
        {
          printf("Tax [%u]:", existing->tax);
          numinput = getInt(&code);
        }
      if (code == 1)
        existing->tax = numinput;
      return existing;
    }
  else
    {
      DatabaseRecord *record = malloc(sizeof(DatabaseRecord));
      printf("Name: ");
      char *input = calloc(12, sizeof(char));
      if (strcmp(fgets(input, 12, stdin), "\n")) //!=
        {
          trim(input);
          strncpy(record->name, input, 11);
        }
      else
        {
          printf("Empty name: exiting\n");
          free(input);
          return NULL;
        }
      free(input);
      input = calloc(22, sizeof(char));
      printf("Middlename: ");
      if (strcmp(fgets(input, 22, stdin), "\n")) //!=
        {
          trim(input);
          strncpy(record->middlename, input, 21);
        }
      else
        memset(record->middlename, 0, 22); //set memory to zero

      free(input);
      input = calloc(22, sizeof(char));
      printf("Surname: ");
      if (strcmp(fgets(input, 22, stdin), "\n"))
        {
          trim(input);
          strncpy(record->surname, input, 21);
        }
      else
        {
          printf("Empty surname: exiting\n");
          free(input);
          return NULL;
        }

      free(input);
      input = calloc(22, sizeof(char));
      printf("Subdivision: ");
      if (strcmp(fgets(input, 22, stdin), "\n")) // !=
        {
          trim(input);
          strncpy(record->subdivision, input, 21);
        }
      else
        memset(record->subdivision, 0, 22); //set memory to zero


      free(input);

      unsigned int numinput;
      int code = 0;
      while (code != 1)
        {
          printf("Salary: ");
          record->salary = getInt(&code);
        }

      code = 0;
      while (code != 1)
        {
          printf("Premium: ");
          record->premium = getInt(&code);
        }

      code = 0;
      while(code != 1)
        {
          printf("Tax: ");
          record->tax = getInt(&code);
        }
      return record;
    }
}


char** command(size_t *size)
{
  char *line = calloc(256, sizeof(char));
  fgets(line, 255, stdin);
  if (!line || !strlen(line))
    return NULL;
  char **res = calloc(1, sizeof(char*));
  *size = 1;
  res[0] = strtok(line, " ");
  if (res[0][strlen(res[0]) - 1] == '\n') //команда без аргументов
    {
      res[0][strlen(res[0]) - 1] = '\0';
    }
  else
    {
      for(;;)
        {
          char *next = strtok(NULL, " ");
          if (!next)
            break;
          (*size)++;
          res = realloc(res, (*size)*sizeof(char*));
          res[*size-1] = next;

          if (next[strlen(next) - 1] == '\n')
            {
              next[strlen(next) - 1] = '\0';
              break;
            }
        }
    }
  return res;
}

void printHelp()
{
  printf("Copyright (c) 2014 Pavel Zinin <pzinin@gmail.com>\n\n");
  printf("Available commands:\n");
  printf("help - print this help\n");
  printf("help <command> - get help on <command>\n");
}

void printUnknown(const char *comm, const char *opt)
{
  printf("%s: unknown option '%s'\n", comm, opt);
}

void printHelpOnModule(const char *module)
{
  if (!strcmp(module,  "quit"))
    printf("quit: Exit from the application\n");
  else if (!strcmp(module,"print"))
    printf("print: print all the entries and ther IDs\n");
  else if (!strcmp(module, "remove"))
    {
      printf("remove <n>: remove record\n");
      printf("       <n> is the position of the record. Use negative value to count from the end");
      printf("\nExample: 'remove -1' removes the last record of the file\n");
    }
  else
    printUnknown("help", module);
}

unsigned int getInt(int *success)
{
  /* success:
     1 - num. entered successfully
     -1 - user pressed just enter
     0 - incorrect input - 0 returned.
     TODO:
     prevent from entering negatives
  */
  errno = 0;
  char *buf = calloc(11, sizeof(char));
  if(!strcmp(fgets(buf, 10, stdin), "\n"))
    {
      *success = -1;
      return 0;
    }
  else
    {
      if(buf[strlen(buf) - 1] != '\n' || strchr(buf,'-'))
        {
          *success = 0;
          return 0;
        }
      char *endptr;
      unsigned int res = (unsigned int)strtoul(buf, &endptr, 10);
      if (endptr == buf) //no results, incorrect input
        {
          *success = 0;
          return 0;
        }
      if ((errno == ERANGE && (res == UINT_MAX))
                   || (errno != 0 && res == 0))
        {
          perror("strtoul");
          *success = 0;
          return 0;
        }
      *success = 1;
      return res;
    }
}



void printchar(char c, int n){
     int i;
     for(i=0;i<n;i++)
         printf("%c",c);
}

void printAllRecords(FILE *db)
{
  //print header
  printchar('-', PRINTSPECLEN);
  printf("\n|%-4s|%-10s|%-20s|%-20s|%-20s|%-10s|%-10s|%-10s|\n","#","Name","Middlename","Surname","Subdivision",
         "Salary", "Premium","Tax");
  printchar('-', PRINTSPECLEN);
  printf("\n");

  //print all the records
  DatabaseRecord *rec;
  rewind(db);
  int n;
  while (rec = nextRecord(db, &n))
    {
     printchar('-', PRINTSPECLEN);
     printf("\n");
     printf(PRINTSPEC,n, rec->name, rec->middlename, rec->surname, rec->subdivision, rec->salary,
            rec->premium, rec->tax);
    }
  printchar('-',PRINTSPECLEN);
  printf("\n");
}


int enterSubstring(char *where, int nch) //shows 'enter query' message
{
  printf("Enter the query:");
  fgets(where, nch, stdin);
  trim(where);
  if (!strlen(where)) //why should I return empty string
    {
      return 0;
    }
  return 1;
}

void search(char **cmd, int size, FILE *db)
{
  if (size == 1)
    printf("find: Not enough arguments\n");
  else if (size == 2 || size == 3)
    {
      int field; //# of column
      int mode; //mode of search
      int numchars;
      char* queryString; unsigned int queryInt;
      if (!strcmp(cmd[1], "name"))
        {
          field = 1; //name - 10 symbols
          numchars = 11;
        }
      else if (!strcmp(cmd[1], "middlename"))
        {
          field = 2;
          numchars = 21;
        }
      else if (!strcmp(cmd[1], "surname"))
        {
          field = 3;
          numchars = 21;
        }
      else if (!strcmp(cmd[1], "subdivision"))
        {
          field = 4;
          numchars = 21;
        }
      else if (!strcmp(cmd[1], "salary"))
        field = 5;
      else if (!strcmp(cmd[1], "premium"))
        field = 6;
      else if (!strcmp(cmd[1], "tax"))
        field = 7;
      else
        {
          printf("find: unknown field: %s\n", cmd[1]);
          return;
        }
      //now parse mode
      switch(field)
        {
        case 1: case 2: case 3: case 4:
          if (size == 2) //no mode specified
            mode = 1; //case-sensitive substring
          else //size == 3
            {
              if (!strcmp(cmd[2], "e")) //exact matching
                mode = 0;
              else if (!strcmp(cmd[2], "s")) //case sensitive matching
                mode = 1;
              // else if (!strcmp(cmd[3], "i")) //case insensitive matching
              // mode = 2;
              else
                {
                  printf("find: unknown mode: %s (see help find)\n", cmd[2]);
                  return;
                }
            }
          //get string from user
          queryString = calloc(numchars, sizeof(char)); //initialize search string
          if(!enterSubstring(queryString, numchars))
            {
              printf("find [%s]: no valid input\n", cmd[1]);
              return;
            }
          break;
        case 5: case 6: case 7:
          if (size == 2) //no mode specified
            mode = 0; //exact matching
          else //size == 3
            {
              if (!strcmp(cmd[2], "<"))
                mode = -2;
              else if (!strcmp(cmd[2], "<="))
                mode = -1;
              else if (!strcmp(cmd[2], "="))
                mode  = 0;
              else if (!strcmp(cmd[2], ">="))
                mode = 1;
              else if (!strcmp(cmd[2], ">"))
                mode = 2;
              else
                {
                  printf("find: unknown mode: %s (see help find)\n", cmd[2]);
                  return;
                }
            }
          //get uint from user
          queryInt = getInt(&numchars); //numchars here is like error detector
          if (numchars != 1)
            {
              printf("find [%s]: no valid input\n", cmd[1]);
              return;
            }
          break;
        }

      rewind(db); //goto start
      int n;  DatabaseRecord* rec;
      if (field > 0 && field <= 4) //string
        rec = findRecord(db, &n, field, mode, queryString);
      else if (field > 4 && field <= 7) //int
        rec = findRecord(db, &n, field, mode, queryInt);

      if (rec)
        {
          printSearchHeader();
          printRecord(rec, n);
          free(rec);
          while (1)
            {
              if (field > 0 && field <= 4) //string
                rec = findRecord(db, &n, field, mode, queryString);
              else if (field > 4 && field <= 7) //int
                rec = findRecord(db, &n, field, mode, queryInt);
              if (!rec)
                break;
              printRecord(rec, n);
              free(rec);
            }
          printSearchTail();
        }
      else
        printf("find [%s]: no results found\n", cmd[1]);

      if (field > 0 && field <= 4) //string
        free(queryString);
    }
  else
    printf("find [%s]: wrong arguments\n", cmd[1]);
}


void printSearchHeader()
{
  printf("Search results:\n");
  printchar('-', PRINTSPECLEN);
  printf("\n|%-4s|%-10s|%-20s|%-20s|%-20s|%-10s|%-10s|%-10s|\n","#","Name","Middlename","Surname","Subdivision",
         "Salary", "Premium","Tax");
  printchar('-', PRINTSPECLEN);
  printf("\n");
}

void printRecord(DatabaseRecord *rec, const int index)
{
  printchar('-', PRINTSPECLEN);
  printf("\n");
  printf(PRINTSPEC, index, rec->name, rec->middlename, rec->surname, rec->subdivision, rec->salary,
         rec->premium, rec->tax);
}

void printSearchTail()
{
  printchar('-',PRINTSPECLEN);
  printf("\n");
}

void sort(char **cmd, int size, FILE *db)
{
  if (size == 1)
    {
      printf("sort: not enough arguments\n");
      return;
    }
  else if (size == 2 || size == 3)
    { //deprecate it and replace with modern code
      int order, type;
      if (!strcmp(cmd[1],"name"))
        type = 1;
      else if (!strcmp(cmd[1], "middlename"))
        type = 2;
      else if (!strcmp(cmd[1], "surname"))
        type = 3;
      else if (!strcmp(cmd[1], "subdivision"))
        type = 4;
      else if (!strcmp(cmd[1], "salary"))
        type = 5;
      else if (!strcmp(cmd[1], "premium"))
        type = 6;
      else if (!strcmp(cmd[1], "tax"))
        type = 7;
      else
        {
          printf("sort: wrong field. See 'help sort'\n");
          return;
        }

      if (size == 3)
        { //determine sort order
          if (!strcmp(cmd[2], "asc")) //ascending
            order = 0;
          else if (!strcmp(cmd[2], "desc")) //descending
            order = 1;
          else
            {
              printf("sort: wrong order. See 'help sort'\n");
              return;
            }
        }
      else if (size == 2)
        {//default sort order depends on type of field
          if (type >= 1 && type <= 4) //strings
            order = 0; //ascending for strings
          else
            order = 1; //descending for numbers
        }
      sortItems(db, order, type);
    }
}
