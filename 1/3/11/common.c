#include "common.h"
FILE* openFile(const char *fileName)
{
  FILE *fd = fopen(fileName, "r+");
  if (!fd)
    {
      if (errno == ENOENT)
        {
          printf("File does not exist, creating it\n");
          if (!(fd = fopen(fileName, "w+")))
            {
              printf("Failed to create a new database: %s\n", strerror(errno));
              return NULL;
            }
          else
            return fd;
        }
      else
        {
          printf("Failed to open the database: %s\n",strerror(errno));
          return NULL;
        }
    }
  return fd;
}

void trim(char *buf)
{
  while((buf[strlen(buf) - 1] == '\n' || buf[strlen(buf) - 1] == ' ') || buf[strlen(buf) - 1] == '\t')
    buf[strlen(buf) - 1] = '\0';

  while((buf[0] == '\n' || buf[0] == ' ') || buf[0] == '\t')
    {
      for (int i = 1; i < strlen(buf); ++i)
        {
          buf[i-1] = buf[i];
        }
      buf[strlen(buf) - 1] = '\0';
    }
}
