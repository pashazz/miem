/* Moscow Institute of Electronics and Mathematics
   The Faculty of Applied Mathematics

   Assignment #3
   Language: C99
   Compiler: gcc

   Student: Pavel Borisov
   Group: Applied Informatics 11
*/


//база данных бухгалтерии
//первым следует код поля
/*
1 name(10) - имя
2 middlename(20) - отчество
3 surname(20) - фамилия
4 subdivision(20) - подразделение
5 salary(uint) - зарплата
6 premium(uint) - премия
7 tax(int) - налог
0 (pseudo) ID
*/
#ifndef _DB_H
#define _DB_H
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <stdarg.h>

typedef struct _DBRECORD
{
  char name[11];
  char middlename[21];
  char surname[21];
  char subdivision[21];
  unsigned int salary;
  unsigned int premium;
  unsigned int tax;
} DatabaseRecord;


/** addRecord
 *
 * adds a record to the end of database. Returns position
 */

int addRecord(FILE* db, DatabaseRecord* record);

/** removeRecord
 *
 * removes a record from the database
 */
void removeRecord(FILE* db, int position);


/** rewriteRecord
 *
 * rewrite record on position.
 * if position is negative, start from the end
 *
 */
int rewriteRecord(FILE* db, int position,const DatabaseRecord *record);

/** findRecord family
 *
 * Returns next record found
 * n is the number of the record
 * type is the code of the column type
 * mode is the mode of search:

 STRINGS
 0 - exact matching
 1 - case-sensitive matching (default)
 2 - case-insensitive matching

 NUMBERS
 -2 - lessthan
 -1 - lessequals
 0 - equals
 1 - moreequals
 2 - more

 3 - error

 the last argument is C string if field is string type, or unsigned int if int type
 */

DatabaseRecord* findRecord(FILE *db, int *n, const int type , const int mode, ...);

//get struct data by field
//last argument is the pointer where data will be placed
char* getStructStringData (DatabaseRecord *rec, int type);
unsigned int getStructIntData (DatabaseRecord *rec, int type);
/*
   -2 - lessthan
   -1 - lessequals
   0 - equals
   1 - moreequals
   2 - more

*/

/* converters to and from absolute position of the file */
long _toAbsolutePosition(int position);


int _fromAbsolutePosition(long position);

/* returns record at position (absolute) or null if not present
   if position is negative, it is calculated from the end */
DatabaseRecord* record(FILE *db, int position);

/* returns the next record in db */
/* if n is not NULL, then it's evaulated to returned record's number */
DatabaseRecord* nextRecord(FILE *db, int *n);


/** freeRecord
 *
 * frees the record initialized by malloc
 */
void freeRecord(DatabaseRecord *rec);

/** sortItems
 *
 * sorts the database by given type
 * order: 0 -> ascending; 1 -> descending
 */

void sortItems (FILE *db, int order, int type);
/* return the count of the records */
int sortCount(FILE *db);
#endif
