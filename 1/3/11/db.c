/* Moscow Institute of Electronics and Mathematics
   The Faculty of Applied Mathematics

   Assignment #3
   Language: C99
   Compiler: gcc

   Student: Pavel Borisov
   Group: Applied Informatics 11
*/

//база данных бухгалтерии
/*
name(10) - имя
middlename(20) - отчество
surname(20) - фамилия
subdivision(10) - подразделение
salary(uint) - зарплата
premium(uint) - премия
tax(int) - налог
*/


#include "db.h"
#include "common.h"

//removeRecord
#include <unistd.h>
#include <sys/types.h>

long _toAbsolutePosition(int position)
{
  return sizeof(DatabaseRecord)*(long)position;
}

int _fromAbsolutePosition(long position)
{
  int pos = (int)(position/sizeof(DatabaseRecord));
  if (position % sizeof(DatabaseRecord) != 0)
    {
      fprintf(stderr, "_fromAbsolutePosition: wrong position: %ld", position);
      assert(0);
    }
  return pos;
 }

//WARNING: usage of POSIX-specific function: ftruncate
void removeRecord(FILE* db, int position)
{
  int flag;
  if (position < 0)
    flag = SEEK_END;
  else
    flag = SEEK_SET;

  size_t pos = _toAbsolutePosition(position);
  fseek(db, pos, flag);
  fseek(db, sizeof(DatabaseRecord), SEEK_CUR);
  while(!feof(db))
    {
      DatabaseRecord* rec = malloc(sizeof(DatabaseRecord));
      if (fread(rec, sizeof(DatabaseRecord), 1, db))
        {
          fseek(db, -2*sizeof(DatabaseRecord), SEEK_CUR);
          fwrite(rec, sizeof(DatabaseRecord), 1, db);
          fseek(db, sizeof(DatabaseRecord), SEEK_CUR);
        }
      free(rec);
    }
  pos = ftell(db) - sizeof(DatabaseRecord);
  ftruncate(fileno(db), pos);
}

int addRecord(FILE* db, DatabaseRecord* record)
{
      fseek(db, 0L, SEEK_END);
      if(fwrite(record, sizeof(DatabaseRecord), 1, db) != 1)
        {
          fprintf(stderr, "addRecord: fwrite error: %s", strerror(errno));
          return -1;
        }
      else
        return _fromAbsolutePosition(ftell(db));

}
void freeRecord(DatabaseRecord *rec)
{
  free(rec);
}

DatabaseRecord* nextRecord(FILE *db, int *n)
{
  DatabaseRecord *rec = malloc(sizeof(DatabaseRecord));
  if (n)
    *n = _fromAbsolutePosition(ftell(db));
  if (fread(rec, sizeof(DatabaseRecord), 1, db))
    return rec;
  else
    {
      free(rec); //not initialized -> no reason to call freeRecord. But malloc is called, so there's reason to call free()
      return NULL;
    }
}

int rewriteRecord(FILE* db, int position, const DatabaseRecord *record)
{
  int flag;
  if (position < 0)
    flag = SEEK_END;
  else
    flag = SEEK_SET;

  fseek(db,_toAbsolutePosition(position), flag);
  if (!fwrite(record, sizeof(DatabaseRecord), 1, db))
    {
      return -1;
    }
  else
    return position;
}
DatabaseRecord* record(FILE *db, int position)
{
  int flag;
  if (position < 0)
    flag = SEEK_END;
  else
    flag = SEEK_SET;

  if(fseek(db, _toAbsolutePosition(position), flag)  == -1)
    {
      fprintf(stderr, "I/O error: wrong position: %d\n", position);
      return NULL;
    }
  DatabaseRecord *rec = malloc(sizeof(DatabaseRecord));
  if (!fread(rec, sizeof(DatabaseRecord),1, db))
    {
      fprintf(stderr, "Failed to read record\n");
      free(rec);
      return NULL;
    }
  return rec;
}


//aux. function for findRecord
int compare(int mode, int type,  ...)
/* the third argument is the value of respective field
   the fourth is the query */
{
  va_list lst;
  va_start(lst, type);
  if (type > 0 && type <= 4) //string
    {
      const char* field = va_arg(lst, const char*); //value of the field
      const char* query = va_arg(lst, const char*); //user's query
      va_end(lst);
      if (mode == 0) //exact matching
        {
          if (!strcmp(field, query))
            return 1;
          else
            return  0;
        }
      else if (mode == 1) //substring
        {
          if (strstr(field, query))
            return 1;
          else
            return 0;
        }
      else
        assert(0);
    }
  else if (type > 4 && type <= 7) //int
    { //todo: error checking
      int field = va_arg(lst, int);
      int query = va_arg(lst, int);
      va_end(lst);
      if ((mode == -2 && field < query) ||
          (mode == -1 && field <= query) ||
          (mode == 0 && field == query) ||
          (mode == 1 && field >= query) ||
          (mode == 2 && field >= query))
        return 1;
      else
        return 0;
    }
  else
    assert(0);
}




DatabaseRecord* findRecord(FILE *db, int *n, const int type, const  int mode, ...)
{ //finds next record
  va_list lst;
  va_start(lst, mode);
  if (type > 0 && type <= 4) //strings
    {
      const char *arg = va_arg(lst, const char *);
      va_end(lst);
      DatabaseRecord *rec;

      while(rec = nextRecord(db, n))
        {
          char *actual = getStructStringData(rec, type);
          if (compare(mode, type, actual, arg))
            return rec;
          else
            free(rec);
        }
      *n = -1;
      return NULL;
    }
 else if(type > 4 && type <= 7) //numbers
    {
      unsigned int arg = va_arg(lst, unsigned int);
      va_end(lst);
      DatabaseRecord *rec;
      while (rec = nextRecord(db, n))
        {
          unsigned int actual;
          actual = getStructIntData(rec, type);
          if (compare(mode, type, actual, arg))
            return rec;
          else
            free(rec);
        }
      *n = -1;
      return NULL;
      }
  else
    assert(0);
}

char* getStructStringData (DatabaseRecord *rec, int type)
{
  if (type == 1)
    return rec->name;
  else if (type == 2)
    return rec->middlename;
  else if (type == 3)
    return rec->surname;
  else if (type == 4)
    return rec->subdivision;

}
unsigned int getStructIntData (DatabaseRecord *rec, int type)
{
  if (type == 5)
    return rec->salary;
  else if (type == 6)
    return rec->premium;
  else if (type == 7)
    return rec->tax;
}


int sortCount(FILE *db)
{
  fseek(db, 0L, SEEK_END);
  return _fromAbsolutePosition(ftell(db));
}




void sortItems(FILE *db, int order, int type)
{
  int count = sortCount(db);
  if (count < 2) //nothing to sort
    return;
  fseek(db, sizeof(DatabaseRecord), SEEK_SET);
  DatabaseRecord *key;
  for (int i = 1; i < count &&  (key = record(db, i)); ++i)
    {
      int j;
      for (j = i; j > 0; --j)
        {
          DatabaseRecord *current = record(db, j - 1);
          if (((type >= 1 && type <= 4) && //string
              ((order == 0 && strcmp(getStructStringData(current, type),
                                     getStructStringData(key, type)) > 0)  //asc order
                  || (order == 1 && strcmp(getStructStringData(current,type),
                                           getStructStringData(key, type)) < 0))) || //desc order
              ((type > 4 && type <= 7) && // int
               ((order == 0 && getStructIntData(current, type) > getStructIntData(key, type)) || //asc order
                order == 1 && getStructIntData(current, type) < getStructIntData(key, type)))) //desc order
                {
                  rewriteRecord(db, j, current);
                  free(current);
                }
              else
                {
                  free(current);
                  break;
                }
            }
      rewriteRecord(db, j, key);
      free(key);
    }
}
