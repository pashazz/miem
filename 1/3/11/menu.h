/* Moscow Institute of Electronics and Mathematics
   The Faculty of Applied Mathematics

   Assignment #11
   Language: C99
   Compiler: gcc

   Student: Pavel Borisov
   Group: Applied Informatics 11
*/


//menu.h - input, output, text-based menu
#ifndef _MENU_H
#define _MENU_H
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include "db.h"


/** command
 *
 * return an array of strings: command and its arguments
 */
char** command(size_t *size);

//the shell implemented as infinite loop
void shell(FILE *fd, const char *fileName);

/* print help message */
void printHelp();


/*prints help on particular module */
void printHelpOnModule(const char *module);

void printUnknown(const char *comm, const char *opt);

/** populateRecord
 *
 * Obtaining record data from stdin.
 * The argument is optional if you want to modify a existing record
 */
DatabaseRecord* populateRecord(DatabaseRecord *existing);

/** getInt
 *
 * returns unsigned int got from the user
 * The 'success' code might be one of:
 * -1 - user pressed enter w/o entering the number
 * 0 - the entered value is incorrect i.e.not a number
 * 1 - the value is correct and returned by the function
 * do not pass NULL to the function
 */
unsigned int getInt(int *success);

/** printAllRecords
 *
 * prints all records in table-like manner
 */
void printAllRecords(FILE *fd);
/** printRecord
 *
 * print record from results of search
 * takes index of the record
 * takes the record itself
 */

void printRecord(DatabaseRecord *rec, const int index);

/* aux. functions */

void printSearchHeader();
void printSearchTail();

/** search
 * parse commandline and perform search
 *
 */
void search(char **cmd, int size, FILE* db);


int enterSubstring(char *where, int nch); //shows 'enter query' message

/** sort
 *
 * sort by field
 */

void sort(char **cmd, int size, FILE *db);

#endif
