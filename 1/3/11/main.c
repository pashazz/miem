/* Moscow Institute of Electronics and Mathematics
   The Faculty of Applied Mathematics

   Assignment #11
   Language: C99
   Compiler: gcc

   Student: Pavel Borisov
   Group: Applied Informatics 11
*/


//main.c
#include "menu.h"
#include "common.h"


void showUsage(const char *progname);


int main(int argc, char* argv[])
{
  if (argc == 2)
    {
      FILE *fd = openFile(argv[1]);
      if (!fd)
        return errno;
      shell(fd, argv[1]);
      fclose(fd);
      return 0;
    }
  else
    {
      showUsage(argv[0]);
      return -1;
    }
}


void showUsage(const char *progname)
{
  printf("База данных бухгалтерии\n\n");
  printf("ИСПОЛЬЗОВАНИЕ\n\t");
  printf("%s <имя файла базы>\n\n", progname);
  printf("Для получения помощи в программе наберите 'help'\n");
  printf("\nДанный файл является частью лабораторной работы #11\n");
}
