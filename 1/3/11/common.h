#ifndef _COMMON_H
#define _COMMON_H
#include <errno.h>
#include <stdio.h>
#include <string.h>
FILE* openFile(const char *fileName);

/* trim the string */
void trim(char *buf);

#endif
