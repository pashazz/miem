#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//lal

void printArray(int, int**);
int* minRow(int, int**, int*);
double med(int, int, int**);
void freeArray(int, int**);
int ** getArray(int);
void printMeds(int*, int, int, int**);
//end lal


int main()
{
  int n;
  printf("Введите порядок матрицы: ");
  scanf("%d", &n);
  if (n<1) return -1;

  int **arr =  getArray(n);
  if (!arr)
    return -1;
  printf("Результат (cр. ар. эл-тов строк с минимумом):\n");
  int size;
  int *nums = minRow(n, arr, &size);
  printMeds(nums, size, n, arr);
  printf("Матрица: \n");
  printArray(n, arr);
  freeArray(n, arr);
  free(nums);
  return 0;
}
void printMeds(int* nums, int nums_size, int matrix_size, int **arr)
{
  for (int i = 0; i<nums_size; ++i)
    {
      printf("Строка %d: %lf\n", nums[i], med(matrix_size, nums[i], arr));
    }
}

int ** getArray(int n)
{
  int **arr =  calloc(n, sizeof(int*));
  for (int i = 0; i<n; ++i)
    {
      *(arr+i) = calloc(n, sizeof(int));
      for (int k = 0; k<n; ++k)
        {
          printf("a[%d][%d]:", i, k);
          if (scanf("%d", *(arr+i)+k) == 0)
            return NULL;
        }
    }
  return arr;
}
void printArray(int n, int **arr)
{
  for (int i = 0; i<n; ++i)
    {
      for (int k = 0; k<n; ++k)
        printf("%d ", arr[i][k]);

      printf("\n");
    }
  printf("\n\n");
}

int* minRow(int n, int **arr, int *size)
{
  int min = arr[0][0];
  int *mrow = calloc(1, sizeof(int));
  *size = 1;
  mrow[0] = 0;
  for (int i = 0; i<n; ++i)
    {
      for (int k = 0; k<n; ++k)
        {
          if (arr[i][k] < min)
            { //set new minimum
              free(mrow);
              mrow = calloc(1, sizeof(int));
              mrow[0] = i;
              *size = 1;
              min = arr[i][k];
            }
          else if(arr[i][k] == min && i != mrow[(*size) - 1])
            { //extend the array
              (*size)++;
              mrow = realloc(mrow, *size*sizeof(int));
              mrow[(*size)-1] = i;
            }
        }
    }
  return mrow;
}

double med(int n, int r, int **arr)
{
  double s = 0.0;
  int *row = *(arr+r);

  for (int i = 0; i<n; ++i)
    s += row[i];
  return (double)s/n;
}

void freeArray(int n, int **arr)
{
  for (int i = 0; i<n; ++i)
    free (*(arr+i));
  free(arr);
}
