#include "a4.h"


int main()
{
  unsigned char a[16], b[16], *c[16];
  // scan_array(a);
  fill_array_randomly(a);

  printf("Array a: ");
  print_array(a);
  int n = doa4(a, b, c);

  printf("Array b: ");
  print_array(b);

  printf("Array c: ");
  for (short i = 0; i < n; i++)
    {
      printf("0x%lX ", c[i]);
    }
  printf("\n");
  printf ("Count of exchanges: %d\n", n);

  return 0;

}

void print_array(unsigned char arr[16])
{
  for (short i = 0; i < 16; i++)
    {
      printf("%hu ", (unsigned short int)arr[i]);
    }
  printf("\n");
}

void fill_array_randomly(unsigned char arr [16])
{
  for (short i = 0; i < 16; i++)
    {
      srand(time(NULL)+i);
      unsigned char random = rand() % 32;
      arr[i] = random;
    }
}


void scan_array(unsigned char arr [16])
{
  printf("Enter 16 byte values in decimal: \n");
  for (short i = 0; i<16; i++)
    {
      unsigned short int t;
      printf("%hu: ", i);
      scanf("%hu", &t);
      arr[i] = (unsigned char)t;
    }
}
