global doa4
;;; 1st rdi
;;; 2nd rsi
;;; 3rd rdx
doa4:
        push rbx
        push rcx
        push rdx

        mov rcx, 16
        xchg rdi, rsi
        mov ebx, 0              ; возвр. кол-во эл-тов
        fill:
                mov al, [rsi]
                cmp al, 9
                jb lessten
	        mov [rdi], al
	        jmp quit
        lessten:
        	mov byte [rdi], 9
	        mov [rdx], rsi
	        add rdx, 8              ;size of pointer address on 64 bit
	        inc ebx
        quit:
        	inc rdi
	        inc rsi
	        loop fill

	        mov eax, ebx
                pop rdx
                pop rcx
                pop rbx

	        ret
