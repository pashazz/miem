/* Moscow Institute of Electronics and Mathematics
   The Faculty of Applied Mathematics

   Assignment #7
   Language: C99
   Compiler: gcc

   Student: Pavel Borisov
   Group: Applied Informatics 11
*/
#include <stdlib.h>
#include <stdio.h>
/** populateMatrix
 *
 * allocate m*n matrix
 * here and further m is row count, n is column count
 */
int** populateMatrix(int m, int n);

/* Convert m*n vector to matrix. Usable for autotests */
void fillMatrixPredef(int **matrix, int m, int n, int *vector);
/** fillMatrixStdin
 *
 * fill m*n matrix with stdin
 */
void fillMatrixStdin(int **matrix, int m, int n);

/** row
 *
 * return i-th row of matrix
 */
int * row(int **matrix, int i);

/** vector
 *
 * return j-th vector (column) of matrix with m dimensions
 *  use it like this: *(arr[k])
 */
int ** vector(int **matrix, int j, int m);

/** printMatrix
 *
 * print formatted matrix to stdin
 */
void printMatrix(int **matrix, int m, int n);


/** maxOfRow
 *
 * return max element of row, where n is # of columns
 */
int maxOfRow(int *row, int n);

/** maxItemRows
 *
 * return an array with numbers of rows with max. item
 * (count is # of such rows)
 */
int* maxItemRows(int **matrix, int m, int n, int *count);

/** minItemSumColumns
 *
 * return an array with numbers of columns with minimal item sum
(count will have written in count)
 */
int* minItemSumColumns(int **matrix, int m, int n, int *count);


/** sortColumn
 *
 * Sort j-th column in matrix using insertion sort
 * m is the row count
 */
void sortColumn(int **matrix, int j, int m);

/** freeMatrix
 *
 * free matrix memory with m rows
 */
void freeMatrix(int **matrix, int m);
/** sumOfVector
 *
 * return sum of vector returned by
 * vector()
 */
int sumOfVector(int **vec, int m);

/** removeRow
 *
 * Remove i-th row from matrix with row count m
 * new row count will have been m-1 after that.
 * Return reallocated matrix
 */
int** removeRow(int **matrix, int m, int i);
