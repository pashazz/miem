/* Moscow Institute of Electronics and Mathematics
   The Faculty of Applied Mathematics

   Assignment #7
   Language: C99
   Compiler: gcc

   Student: Pavel Borisov
   Group: Applied Informatics 11
*/

#include "matrix.h"

int main()
{
  int m, n;
  printf("Enter the # of rows in a  matrix: ");
  scanf("%d", &m);

  printf("Enter the # of columns in a matrix: ");
  scanf("%d", &n);
  int **matr = populateMatrix(m, n);
  fillMatrixStdin(matr, m, n);

  //find rows with max elem.
  int c; //count of (removed) rows
  int *rows = maxItemRows(matr, m, n, &c);
  for (int i = 0; i < c; i++)
    {
      matr = removeRow(matr, m, rows[i]-i); //Consider row will have been moved left every time after deletion so -i.
    }
  m -= c;

  printf("%d row(s) with maximum element(s) have been removed.\n", c);
  printMatrix(matr, m, n);
  freeMatrix(matr, m);
}
