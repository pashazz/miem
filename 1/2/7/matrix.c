/* Moscow Institute of Electronics and Mathematics
   The Faculty of Applied Mathematics

   Assignment #7
   Language: C99
   Compiler: gcc

   Student: Pavel Borisov
   Group: Applied Informatics 11
*/

#include "matrix.h"

int** populateMatrix(int m, int n)
{
  int** matrix = calloc(m, sizeof(int*));
  for (int i = 0; i < m; ++i)
    {
      matrix[i] = calloc(n, sizeof(int));
    }
  return matrix;
}

void fillMatrixPredef(int **matrix, int m, int n, int *vector)
{
  int pos = 0;
  for (int i = 0; i < m; ++i)
    for (int j = 0; j < n; ++j, ++pos)
      matrix[i][j] = vector[pos];
}

void fillMatrixStdin(int **matrix, int m, int n)
{
  for (int i = 0; i < m; ++i)
    for (int j = 0; j < n; ++j)
      {
        printf("(%d, %d): ", i, j);
        scanf("%d", matrix[i]+j);
      }
}

int * row(int **matrix, int i)
{
  return matrix[i];
}

int ** vector(int **matrix, int m, int j)
{
  int **vec = calloc(m, sizeof(int*));
  for (int i = 0;i< m; ++i)
    vec[i] = matrix [i]+j;
  return vec;
}

void printMatrix(int **matrix, int m, int n)
{
  printf("[\n");
  for (int i = 0; i < m; ++i)
    {
    for (int j = 0; j < n; ++j)
      {
        printf("%d", matrix[i][j]);
        if (j != n - 1)
          printf(" ");
      }
    printf("\n");
    }
  printf("]\n");
}

int maxOfRow(int *row, int n)
{
  int max = row[0];
  for (int i = 1; i < n; ++i)
    if (row[i] > max)
      max = row[i];
  return max;
}

int* maxItemRows(int **matrix, int m, int n, int *count)
{
  int max = maxOfRow(matrix[0], n);
  int *ret = calloc(m, sizeof(int));
  ret[0] = 0;
  *count  = 1;

  for (int i = 1; i < m; ++i)
    {
      int currMax = maxOfRow(matrix[i], n);
      if (currMax > max)
        {
          *count = 1;
          ret[0] = i;
          currMax = max;
        }
      else if (currMax == max)
        {
          ret[*count] = i;
          (*count)++;
        }
    }
  ret = realloc(ret, (*count)*sizeof(int));
  return ret;
}

void sortColumn(int **matrix, int j, int m)
{
  int **vec  = vector(matrix, m, j);
  for (int i = 1; i < m; ++i)
    {
      int key = *(vec[i]);
      int k;
      for (k = i-1; k >=0; k--)
        {
          if (key < *(vec[k]))
            *(vec[k+1]) = *(vec[k]);
          else
            {
              *(vec[k+1]) = key;
              break;
            }
        }
      if (k == -1)
        *(vec[0]) = key;
    }
  free(vec);
}

void freeMatrix(int **matrix, int m)
{
  for (int i = 0; i < m; i++)
    free(matrix[i]);
  free(matrix);
}

int sumOfVector(int **vec, int m)
{
  int sum  = 0;
  for (int i = 0; i < m; i++)
    sum += *vec[i];
  return sum;
}

int* minItemSumColumns(int** matrix, int m, int n, int* count)
{
  int * ret = calloc(n, sizeof(int));
  int** vec = vector(matrix, m, 0);
  int min = sumOfVector(vec, m);
  free(vec);
  ret[0] = 0;
  *count = 1;

  for (int i = 1; i < n; ++i)
    {
      vec = vector(matrix, m, i);
      int currMin = sumOfVector(vec, m);
      free(vec);
      if (currMin == min)
        {
          ret[*count] = i;
          (*count)++;
        }
      else if (currMin < min)
        {
          ret[0] = i;
          *count = 1;
        }
    }
  return ret;
}

int** removeRow(int **matrix, int m, int i)
{
  for (int j = i; j < m - 1; j++)
    matrix[j] = matrix[j+1];
  return realloc(matrix, (m-1)*sizeof(int*));
}
