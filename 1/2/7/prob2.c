/* Moscow Institute of Electronics and Mathematics
   The Faculty of Applied Mathematics

   Assignment #7
   Language: C99
   Compiler: gcc

   Student: Pavel Borisov
   Group: Applied Informatics 11
*/

#include "matrix.h"

/* Given n*n matrix
   Sort columns with minimal sum */

int main()
{
  printf("Enter the range of square matrix: ");
  int n;
  scanf("%d", &n);

  printf("Enter the matrix itself:\n");
  int **matr = populateMatrix(n, n);
  fillMatrixStdin(matr, n, n);

  int c;
  int *minColumns = minItemSumColumns(matr, n, n, &c);
  for (int i = 0; i < c; ++i)
    {
      sortColumn(matr, minColumns[i], n);
    }
  printMatrix(matr, n, n);
}
