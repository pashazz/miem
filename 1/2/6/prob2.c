/* Moscow Institute of Electronics and Mathematics
   The Faculty of Applied Mathematics

   Assignment #6
   Language: C99
   Compiler: gcc

   Student: Pavel Borisov
   Group: Applied Informatics 11
*/
#include "array_int.h"
int* intersection(int* a1, int a1size, int* a2, int a2size, int nomorethan, int *retSize);
int isIn(int *array, int size, int value);
int numberOfOccurences(int *array, int size,  int value);

int main()
{
  int m, n;
  printf("Enter the size of a:");
  scanf("%d", &m);
  printf("Enter the size of b:");
  scanf("%d", &n);
  printf("Array A is int. ");
  int *a = populateArray(m);
  printf("Array B is int. ");
  int *b = populateArray(n);
  int csize;
  int *c = intersection(a, m, b, n, 3, &csize);
  printf("Result: ");
  printArray(csize, c);
  printf("\n");
  return 0;
}
/** intersection (int*, int, int*, int, int)
 *
 * return int array of size min(a1size, a2size)
 * containing an intersection of arrays, with nomorethan is maximum number
 * of occurences of each number in the array
 * size of returned array is passed through retSize pointer
 * if there is no intersection, return NULL
 */
int* intersection(int *a1, int a1size, int* a2, int a2size, int nomorethan, int *retSize)
{
  int* masterArr = (a1size > a2size ? a1 : a2);
  int* slaveArr = (masterArr == a2 ? a1 : a2);
  int masterSize = (a1size > a2size ? a1size : a2size);
  int slaveSize = (masterSize == a2size ? a1size : a2size);

  int *ret = calloc(slaveSize, sizeof(int));
  *retSize = 0;
  insertSort(masterSize, masterArr);
  insertSort(slaveSize, slaveArr);

 //curr - current repeating element, j = # of occurences in slave
  for (int i = 0; i < slaveSize;)
    {
      int curr = slaveArr[i];
      int j = 1;
      for (++i; (i < slaveSize && slaveArr[i] == curr); ++i, ++j); // j will be the # of occurences of the number
      if (j <= nomorethan)
        {
          int occInMaster = numberOfOccurences(masterArr, masterSize, curr);
          if (occInMaster > 0 && occInMaster <= nomorethan)
            {
              ret[*retSize] = curr;
              (*retSize)++;
            }
        }
    }
  if (*retSize== 0)
    { //unallocate array, return NULL
      free(ret);
      return NULL;
    }
  else if (*retSize == slaveSize)
    { // slaveArr is subset of masterArr
      return ret;
    }
  else
    { //reallocate to new size
      ret = realloc(ret, *retSize*sizeof(int));
      return ret;
    }
}




/** numberOfOccurences (int*, int, int)
 * return number of occurrences (obviously)
 *
 */
int numberOfOccurences(int *array, int size,  int value)
{
  for (int i = 0; i<size; ++i)
    {
      if (array[i] == value)
            {
              int occurences = 1;
              for (++i;(i < size  && array[i] == value); ++i, ++occurences);
              return occurences;
            }
    }
  return 0;
}
