/* Moscow Institute of Electronics and Mathematics
   The Faculty of Applied Mathematics

   Assignment #6
   Language: C99
   Compiler: gcc

   Student: Pavel Borisov
   Group: Applied Informatics 11
*/

#include "array_double.h"


/** removeElements
 *
 * remove elements from double array with *n length. New length will have been placed in n after removal
 * return reallocated array
 */
double* removeElements(int *n, double *arr);

void moveLeft(int n, double *arr, int dest); //moves remaining items of arr by 1 after dest. Thus,
                                             //dest will be removed

int main()
{
  int len;
  printf("Enter the size of an array: ");
  scanf("%d", &len);
  printf("\n");
  double *arr = populateArray(len);
  int n = len;
  arr = removeElements(&n, arr);
  printf("Result: removed %d elements\nArray: ", len-n);
  printArray(n, arr);
  free(arr);
}

double* removeElements(int *n, double *arr)
{ /* remove positive elements after first maximum */
  int i =  maxIndex(*n, arr, 1) + 1;
  while (i < *n)
    {
      if (arr[i] > 0)
        {
          moveLeft(*n, arr, i);
          (*n)--; // shifting an array
        }
      else
        i++;
    }
  arr = realloc(arr, *n*sizeof(double));
  return arr;
}

void moveLeft(int n, double *arr, int dest)
{
  for (int i = dest; i < n - 1; ++i)
    arr[i] = arr[i+1];
  arr[n-1] = 0;
}
