/* Moscow Institute of Electronics and Mathematics
   The Faculty of Applied Mathematics

   Assignment #6
   Language: C99
   Compiler: gcc

   Student: Pavel Borisov
   Group: Applied Informatics 11
*/
#include <stdlib.h>
#include <stdio.h>

double * populateArray(int n); //create a new array with calloc, fill
                               //it with stdin

void printArray(int n, double* arr); //print the array in the square
                                     //brackets

int maxIndex(int n, double *arr, int first); //find maximum index of
                                             //the array
