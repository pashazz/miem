/* Moscow Institute of Electronics and Mathematics
   The Faculty of Applied Mathematics

   Assignment #6
   Language: C99
   Compiler: gcc

   Student: Pavel Borisov
   Group: Applied Informatics 11
*/

#include "array_double.h"


/* quick attempt to implement == operation for doubles*/
#include <math.h>
#define EPS 0.0001
#define equ(x, y) fabs(x - y) < EPS

double * populateArray(int n)
{
  printf("Enter %d elements of the array\n", n);
  double *arr = calloc(n, sizeof(double));
  for (int i = 0; i < n; ++i)
    {
      printf("[%d]", i);
      scanf("%lf", arr+i);
    }
  return arr;
}

void printArray(int n, double* arr)
{
  printf ("[");
  for (int i = 0; i < n; ++i)
    {
      printf("%.2lf", arr[i]);
      if (i != n - 1)
        printf(" ");
    }
  printf("]\n");
}

int maxIndex(int n, double* arr, int first)
{
  double max = arr[0];
  int maxi = 0;

  for (int i = 1; i < n; ++i)
    {
      if (arr[i] > max || (!first && equ(arr[i], max)))
        {
          maxi = i;
          max = arr[i];
        }
    }
  return maxi;
}
