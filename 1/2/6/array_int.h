/* Moscow Institute of Electronics and Mathematics
   The Faculty of Applied Mathematics

   Assignment #6
   Language: C99
   Compiler: gcc

   Student: Pavel Borisov
   Group: Applied Informatics 11
*/
#include <stdlib.h>
#include <stdio.h>
int * populateArray(int n); //create a new array with calloc, fill
                               //it with stdin

void printArray(int n, int* arr); //print the array in the square brackets
void insertSort(int n, int *a); //insertion sort
