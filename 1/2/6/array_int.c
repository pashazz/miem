/* Moscow Institute of Electronics and Mathematics
   The Faculty of Applied Mathematics

   Assignment #6
   Language: C99
   Compiler: gcc

   Student: Pavel Borisov
   Group: Applied Informatics 11
*/

#include "array_int.h"

int * populateArray(int n)
{
  printf("Enter %d elements of the array\n", n);
  int *arr = calloc(n, sizeof(int));
  for (int i = 0; i < n; ++i)
    {
      printf("[%d]", i);
      scanf("%d", arr+i);
    }
  return arr;
}

void printArray(int n, int* arr)
{
  printf ("[");
  for (int i = 0; i < n; ++i)
    {
      printf("%d", arr[i]);
      if (i != n - 1)
        printf(" ");
    }
  printf("]\n");
}

void insertSort(int n, int *a)
{
  for (int i = 1; i < n; ++i)
    {
      int key = a[i], j;
      for (j = i; j>0; --j)
        if (a[j-1] > key)
          a[j] = a[j-1];
        else
          break;
      a[j] = key;
    }
}
