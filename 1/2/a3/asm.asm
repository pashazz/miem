;;; A3 assembly code (external C function)
;;;

section .text

global doa3



doa3:
push rax
push rbx
push rcx


mov al, [rdi]
sub al, 30h
mov bl, [rdi+1]
sub bl, 30h
mov cl, [rdi+2]
sub cl, 30h

mul cl                         ; I know that result will be 81d or
                                ; less, so here we have bits set on
                                ; al only!
cmp al, bl
jne fill

add bl, 2
cmp bl, 10
jae quit

        ;; bx -> letter
add bl, 30h

mov [rdi+1], bl
jmp quit

fill:
        mov al, '9'
        mov [rdi], al
        mov [rdi+1], al
        mov [rdi+2], al

quit:
        pop rcx
        pop rbx
        pop rax
        ret
