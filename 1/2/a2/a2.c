#include <stdio.h>

/* Assembly A2. Compile: make
3 hexadecimals on input
1 hexadecimal on output
Byte represented by char (sizeof(char) == 1)
Word represented by short (sizeof(short) == 2)
*/

extern short calculate_exp(short, char, char);

main()
{
  short x, y_inp, z_inp;
  printf("Enter the folowing values in the hexadecimal format:\n");
  printf("x: ");
  scanf("%hx", &x);
  printf("y: ");
  scanf("%hx", &y_inp);
  printf("z: ");
  scanf("%hx", &z_inp);
  short res = calculate_exp(x,(char)y_inp,(char)z_inp);
  //Here we convert short -> char, we assume that y and z is small
  //enough

  printf("Result (unsigned): %hx\n", res); //C does not have a feature
                                           //of printing signed hex
}
