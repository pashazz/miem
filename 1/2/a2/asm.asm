global calculate_exp

;;; By X86_64 calling convention
;;; First argument in RDI
;;; Second in RSI
;;; Third in RDX
;;; More: http://wiki.osdev.org/Calling_Conventions

;;; Expression:
;;; \nu = \frac{(z-3)x+1}{y-1}+1
calculate_exp:
        push rbx                ; save that regs by calling convention
        push rcx
        push rdx
        xchg al,dl              ; Unnecessary, but to simulate 16bits situation
        cbw
        xchg ax, bx
        sub bx,3
        mov ax, di              ;x
        imul bx                 ;result in dx: ax
        add ax, 1               ;cant use inc since it does not change cf
        adc dx, 0
        mov bx, si             ; Y is byte but si doesnt have byte register
        xchg bx, ax             ; convert it to word
        cbw
        xchg bx, ax             ; again
        dec bx                  ; y-1
        idiv bx                  ;finally
        inc ax                  ; return short value in rax(ax)
        pop rdx
        pop rcx
        pop rbx


        ret
