# My works at Moscow University of Electronics and Mathematics

The structure of directories consists of:
1. A grade level
2. A module
3. An identifier

Each program could be compiled with corresponding Makefile.
Toolchain used:
* GNU make
* GNU gcc
* nasm

Makefiles could be used only on linux since elf64 output is used by
nasm there
